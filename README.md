# CarCar

Team:

* Chris - Services
* Joyce - Sales

## Design

## Service microservice

Developed react forms integrated with APIs to post instances of new vehicle models and manufacturers. Render the list of models and manufacturers using react integrated with API response. Create forms for technician and vehicle servicing to create new technicians and service appointments, integrated with API response to render a list of technicians and service appointments. Use the automobile API to filter vehicle VIN data from the database to show if customer received VIP services for vehicles purchased from inventory. Implemented search bar via React to filter service history data by VIN number.

## Sales microservice

In the Sales microservice I created four models. Through the usage of the inventory microservice, I was  able to access the necessary fields in order to create my own AutomobileVO in my models. This model allows for me to access the vin and sold status of the automobiles. The other three models I created were the Salesperson, customer, and sale models that I then used in my views to create the backend. My sale model held three foreign keys (salesperson, automobileVo, and customer) in order to connect a sale to its customer, salesperson, and the unique car. These models all contained charfields and integer fields in order to allow for input.
