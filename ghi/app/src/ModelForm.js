import React, {useEffect, useState} from 'react';


function ModelForm(){
  const [modelName, setModelName] = useState('');
  const [image, setImage] = useState('');
  const [manufacturer, setManufacturer] = useState('');
  const [manufacturers, setManufacturers] = useState([]);

    const handleSubmit = async(event)=>{
        event.preventDefault();
        const data = {};
        data.name = modelName;
        data.picture_url = image;
        data.manufacturer_id = manufacturer;

        const modelUrl = 'http://localhost:8100/api/models/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            header: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(modelUrl, fetchConfig);
        if (response.ok){
            setModelName('');
            setImage('');
            setManufacturer('');
        }
    }

    const handleModelNameChange = (event) => {
        const value = event.target.value;
        setModelName(value);
    }

    const handleImageChange = (event) => {
        const value = event.target.value;
        setImage(value);
    }

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const fetchData = async() => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);

        if (response.ok){
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }

    useEffect(()=>{
        fetchData();
    },[]);

    return(
        <>
        <div className="container">
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a vehicle model</h1>
                <form onSubmit={handleSubmit} id="create-model-form">
                  <div className="form-floating mb-3">
                    <input value={modelName} onChange={handleModelNameChange} placeholder="Model Name" required type="text" id="name" name="name" className="form-control"/>
                    <label htmlFor="name">Model Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input value={image} onChange={handleImageChange} placeholder="Picture URL" required type="url" id="picture_url" name="picture_url" className="form-control"/>
                    <label htmlFor="picture_url">Picture URL</label>
                  </div>
                  <div className="mb-3">
                    <select value={manufacturer} onChange={handleManufacturerChange} required id="manufacturer" name="manufacturer" className="form-select">
                      <option >Choose a manufacturer</option>
                      {manufacturers.map(m=>{
                        return(
                            <option key={m.href} value={m.id}>
                                {m.name}
                            </option>
                        )
                      })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        </div>
        </>
        );
}

export default ModelForm;
