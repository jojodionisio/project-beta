import React, { useEffect, useState } from 'react';

function Technicians() {
    const [techState, setTechState] = useState([]);

    const loadTechs = async () => {
        const response = await fetch('http://localhost:8080/api/technicians/');
        if (response.ok){
            const data = await response.json();
            setTechState(data.technicians)
        } else {
            console.error(response)
        }
    }
    useEffect(()=>{
        loadTechs();
    }, []);

    return (
      <>
        <h1>Technicians</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Employee Id</th>
              <th>First Name</th>
              <th>Last Name</th>
            </tr>
          </thead>
          <tbody>
            {techState.map(tech => {
              return (
                <tr key={tech.id}>
                  <td>{tech.employee_id}</td>
                  <td>{tech.first_name}</td>
                  <td>{tech.last_name}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </>
    );
}

export default Technicians;
