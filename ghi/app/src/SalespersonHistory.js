import React, { useEffect, useState } from 'react';

function SalespersonHistoryList() {
    const [salespeople, setSalespeople] = useState([]);
    const [salesperson, setSalesperson] = useState('');
    const [sales, setSales] = useState([]);

    const fetchSales = async () => {
        const salesUrl = 'http://localhost:8090/api/sales/';
        const response = await fetch(salesUrl)
        if (response.ok) {
            const data = await response.json();
            const salesFiltered = data.sales.filter(sales => sales.salesperson.employee_id.includes(salesperson));
            setSales(salesFiltered)
        }
    }


    function handleSalesPersonChange(e) {
        const value = e.target.value;
        setSalesperson(value);
    }

    useEffect(() => {
        const loadSalespeople = async () => {
            const response = await fetch('http://localhost:8090/api/salespeople/');
            if (response.ok) {
                const data = await response.json();
                setSalespeople(data.salespeople);
            }
        };
        loadSalespeople();
    }, []);

    useEffect(() => {
        fetchSales()
    }, [salesperson]);

    return (
        <>
            <div className="container">
                <h1> Salesperson History</h1>
                <div className='row'>
                    <form id='form-search' name="form-search" method="get" action="" className="form-inline">
                        <div className='form-group'>
                            <div className='select-group'>
                                <select onChange={handleSalesPersonChange} value={salesperson} required className='form-select' id='salesperson'>
                                    <option value="">Choose a Salesperson...</option>
                                    {salespeople.map(salesperson => {
                                        return (
                                            <option key={salesperson.id} value={salesperson.employee_id}>{salesperson.first_name} {salesperson.last_name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <table className='table table-striped'>
                    <thead>
                        <tr>
                            <th>Salesperson</th>
                            <th>Customer Name</th>
                            <th>VIN</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {sales?.map((sale) => {
                            return (
                                <tr key={sale.id}>
                                    <td>{`${sale.salesperson.first_name} ${sale.salesperson.last_name}`}</td>
                                    <td>{`${sale.customer.first_name} ${sale.customer.last_name}`}</td>
                                    <td>{sale.automobile.vin}</td>
                                    <td>${sale.price}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </>
    )
}

export default SalespersonHistoryList;
