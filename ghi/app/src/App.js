import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import CustomerList from './CustomerList';
import CustomerForm from './CustomerForm';
import SalespeopleList from './SalespersonList';
import SalespersonForm from './SalespersonForm';
import SalesList from './SalesList';
import SalesForm from './SalesForm';
import SalespersonHistoryList from './SalespersonHistory';
import AutomobileForm from './AutomobileForm';
import AutomobileList from './AutomobileList';
import TechnicianForm from './TechnicianForm';
import Technicians from './Technicians';
import AppointmentForm from './AppointmentForm';
import AppointmentsList from './AppointmentsList';
import ServiceHistory from './ServiceHistory';
import ManufacturerForm from './ManufacturerForm';
import Manufacturers from './Manufacturers';
import ModelForm from './ModelForm';
import Models from './Models';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="salespeople">
            <Route index element={<SalespeopleList />} />
            <Route path="create" element={<SalespersonForm />} />
          </Route>
          <Route path="sales">
            <Route index element={<SalesList />} />
            <Route path="create" element={<SalesForm />} />
            <Route path="history" element={<SalespersonHistoryList />} />
          </Route>
          <Route path="customers">
            <Route index element={<CustomerList />} />
            <Route path="create" element={<CustomerForm />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobileList />} />
            <Route path="create" element={<AutomobileForm />} />
          </Route>
          <Route path="manufacturers">
            <Route index element={<Manufacturers />} />
            <Route path="create" element={<ManufacturerForm />} />
          </Route>
          <Route path="models">
            <Route index element={<Models />} />
            <Route path="create" element={<ModelForm />} />
          </Route>
          <Route path="technicians">
            <Route index element={<Technicians />} />
            <Route path="create" element={<TechnicianForm />} />
          </Route>
          <Route path="appointments">
            <Route index element={<AppointmentsList />} />
            <Route path="create" element={<AppointmentForm />} />
            <Route path="history" element={<ServiceHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
