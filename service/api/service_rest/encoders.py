from common.json import ModelEncoder
from .models import Technician, Appointment, AutomobileVO


class AutomobileVoDetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "vin",
        "sold",
        "id"
    ]


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id"
    ]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "id"
    ]

    def get_extra_data(self, o):
        return {
            "technician": o.technician.last_name,
            "employee_id": o.technician.employee_id
        }
